package com.mnemo.spark.lib.tests.unit;

import com.mnemo.util.DataFrameUtils;
import com.mnemo.util.h2.service.EmbeddedH2Service;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jose on 16/01/17.
 */
public class SqlCassandraSourcesTests {

    private static SparkConf sparkConf;
    private static SparkContext sparkContext;
    private static SQLContext sparkSqlContext;

    @BeforeClass
    public static void init() throws ClassNotFoundException {
        sparkConf = new SparkConf().setMaster("local").setAppName("SqlCassandraSourcesTests");
        sparkConf.set("spark.cassandra.connection.host", "127.0.0.1");
        sparkConf.set("spark.cassandra.connection.port", "9042");
        sparkContext = new SparkContext(sparkConf);
        sparkSqlContext = new SQLContext(sparkContext);

        // Add h2 database
        DataFrame df = sparkSqlContext.read().jdbc(EmbeddedH2Service.URL, EmbeddedH2Service.TABLE, new Properties());
        df.registerTempTable(EmbeddedH2Service.TABLE);
        Class.forName(org.h2.Driver.class.getName());
        sparkContext.addJar(SparkContext.jarOfClass(org.h2.Driver.class).get());

        // Add cassandra database
        df = sparkSqlContext.read().format("org.apache.spark.sql.cassandra").option("table", "cstable")
                .option("keyspace", "test_keyspace").load();
        df.registerTempTable("cstable");
        sparkContext.addJar(SparkContext
                .jarOfClass(com.datastax.spark.connector.rdd.partitioner.CassandraPartition.class).get());
    }

    @AfterClass
    public static void shutdown() {
        if (sparkContext != null) {
            sparkContext.cancelAllJobs();
            sparkContext.stop();
        }
        sparkContext = null;
        sparkSqlContext = null;
        sparkConf = null;
    }

    @Test
    public void testStructure() {
        final DataFrame df = sparkSqlContext.sql("show tables");
        final List<Map<String, Object>> rows = DataFrameUtils.toListMap(df);
        Assert.assertEquals("Two rows expected", 2, rows.size());
        final HashSet<String> tables = new HashSet<String>();
        tables.add("h2table");
        tables.add("cstable");
        String t = (String)rows.get(0).get("tableName");
        Assert.assertTrue("Table " + t + " unexpected", tables.remove(t) );
        t = (String)rows.get(1).get("tableName");
        Assert.assertTrue("Table " + t + " unexpected", tables.remove(t) );
        Assert.assertEquals("Table 1 must be temporary", "true", rows.get(0).get("isTemporary").toString());
        Assert.assertEquals("Table 2 must be temporary", "true", rows.get(1).get("isTemporary").toString());
    }

    @Test
    public void testJoin() {
        final DataFrame df = sparkSqlContext.sql("select cs.id csid, cs.val csval, h.id h2id, h.val h2val from h2table h inner join cstable cs on h.id=cs.id order by cs.id asc, cs.val asc");
        final List<Map<String, Object>> rows = DataFrameUtils.toListMap(df);
        Assert.assertEquals("Two rows expected", 2, rows.size());
        Assert.assertEquals("row[0].csid must be 1", "1", rows.get(0).get("csid").toString());
        Assert.assertEquals("row[0].h2id must be 1", "1", rows.get(0).get("h2id").toString());
        Assert.assertEquals("row[0].csval must be 'cs1'", "cs1", rows.get(0).get("csval").toString());
        Assert.assertEquals("row[0].h2val must be 'h21'", "h21", rows.get(0).get("h2val").toString());
        Assert.assertEquals("row[1].csid must be 2", "2", rows.get(1).get("csid").toString());
        Assert.assertEquals("row[1].h2id must be 2", "2", rows.get(1).get("h2id").toString());
        Assert.assertEquals("row[1].csval must be 'cs2'", "cs2", rows.get(1).get("csval").toString());
        Assert.assertEquals("row[1].h2val must be 'h22'", "h22", rows.get(1).get("h2val").toString());
    }

}
