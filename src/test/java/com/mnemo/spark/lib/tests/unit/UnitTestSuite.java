package com.mnemo.spark.lib.tests.unit;

import com.mnemo.util.cassandra.service.EmbeddedCassandraService;
import com.mnemo.util.h2.service.EmbeddedH2Service;
import org.apache.thrift.transport.TTransportException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

/**
 * Created by jose on 16/01/17.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        SqlCassandraSourcesTests.class
})
public class UnitTestSuite {

    private static EmbeddedCassandraService embeddedCassandraService;
    private static EmbeddedH2Service embeddedH2Service;

    @BeforeClass
    public static void setUp() throws IOException, TTransportException {
        // Start H2 and cassandra databases
        embeddedH2Service = new EmbeddedH2Service();
        embeddedH2Service.start();
        embeddedCassandraService = new EmbeddedCassandraService();
        embeddedCassandraService.start();
    }

    @AfterClass
    public static void tearDown() {
        if (embeddedCassandraService!=null) embeddedCassandraService.stop();
        if (embeddedH2Service!=null) embeddedH2Service.stop();
    }



}
