package com.mnemo.util.cassandra.service;
 
import java.io.File;
import java.io.IOException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.mnemo.util.FileUtils;
import org.apache.cassandra.service.CassandraDaemon;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
/**
 * An embedded, in-memory cassandra storage service that listens
 * on the thrift interface as configured in storage-conf.xml
 * This kind of service is useful when running unit tests of
 * services using cassandra for example.
 *
 
 * This is the implementation of https://issues.apache.org/jira/browse/CASSANDRA-740
 *
 
 * How to use:
 * In the client code create a new thread and spawn it with its {@link Thread#start()} method.
 * Example:
 *
 *      // Tell cassandra where the configuration files are.
        System.setProperty("storage-config", "conf");
 
        cassandra = new EmbeddedCassandraService();
        cassandra.init();
 
        // spawn cassandra in a new thread
        Thread t = new Thread(cassandra);
        t.setDaemon(true);
        t.start();
 
 *
 * @author Ran Tavory (rantav@gmail.com)
 *
 */
public class EmbeddedCassandraService implements Runnable
{

    final static Logger logger = LoggerFactory.getLogger(EmbeddedCassandraService.class);
 
    CassandraDaemon cassandraDaemon;
    Thread t;
 
    public void start() throws TTransportException, IOException
    {
        FileUtils.delete( new File("./target/cassandra/") );

        System.setProperty("cassandra.storagedir", "./target/cassandra/storage");

        cassandraDaemon = new CassandraDaemon();
        cassandraDaemon.init(null);

        t = new Thread(this);
        t.setDaemon(true);
        t.start();

        createTestData();
    }

    private void createTestData() {
        logger.info("> Create test data on Cassandra");
        Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1")
                .withPort(9042).build();
        try {
            final Session session = cluster.newSession();
            session.execute("create keyspace test_keyspace WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };");
            session.execute("use test_keyspace");
            session.execute("create table cstable(id int primary key, val varchar)");
            session.execute("insert into cstable(id, val) values(1,'cs1')");
            session.execute("insert into cstable(id, val) values(2,'cs2')");
            session.close();
        }
        finally {
            cluster.close();
            cluster=null;
        }
    }

    @Override
    public void run()
    {
        cassandraDaemon.start();
    }

    public void stop() {
        if (cassandraDaemon==null) return;
        try {
            cassandraDaemon.stop();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        try {
            t.join(10000l);
            if (t.isAlive()) {
                t.interrupt();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}