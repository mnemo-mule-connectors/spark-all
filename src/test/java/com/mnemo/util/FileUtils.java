package com.mnemo.util;

import java.io.File;

/**
 * Created by jose on 17/01/17.
 */
public class FileUtils {

    public static void delete(final File dirFile) {
        if (dirFile.exists() && dirFile.isDirectory()) {
            final File[] files = dirFile.listFiles();
            if (files!=null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        delete(f);
                    }
                    else {
                        f.delete();
                    }
                }
            }
            dirFile.delete();
        }
    }
}
