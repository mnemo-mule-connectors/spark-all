package com.mnemo.util.h2.service;

import com.mnemo.util.FileUtils;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by jose on 16/01/17.
 */
public class EmbeddedH2Service {

    final static Logger logger = LoggerFactory.getLogger(EmbeddedH2Service.class);

    public static final String URL = "jdbc:h2:tcp://localhost/./tests;DATABASE_TO_UPPER=false";
    public static final String TABLE = "h2table";
    private static final String DB_PATH="./target/h2";

    Server server;

    public void start() {

        FileUtils.delete( new File(DB_PATH) );

        try {
            server = Server.createTcpServer("-baseDir", DB_PATH, "-tcpAllowOthers").start();
            // Example URL jdbc:h2:tcp://localhost/~/stackoverflow
            // sa/
        } catch (Exception e) {
            e.printStackTrace();

        }
        try {
            createTestData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void createTestData() throws SQLException {
        logger.info("> Create test data on H2");
        final Connection conn = DriverManager.getConnection(URL);
        try{
            conn.prepareCall("create table h2table(id int primary key, val varchar)").execute();
            conn.prepareCall("insert into h2table(id, val) values(1,'h21')").execute();
            conn.prepareCall("insert into h2table(id, val) values(2,'h22')").execute();
            conn.commit();
        }
        finally {
            conn.close();
        }
    }

    public void stop() {
        if (server!=null) server.stop();
    }
}
