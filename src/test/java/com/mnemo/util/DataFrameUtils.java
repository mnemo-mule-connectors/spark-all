package com.mnemo.util;

import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;

import java.util.*;

/**
 * Created by jose on 17/01/17.
 */
public class DataFrameUtils {

    public static List<Map<String, Object>> toListMap(final DataFrame df) {
        final String[] colNames = df.columns();
        final List<Map<String, Object>> list = new ArrayList<>();
        final List<Row> rowList = df.collectAsList();
        if (rowList != null) {
            for (Row r : rowList) {
                final Map<String, Object> map = new HashMap<>();
                for (int i = 0; i < colNames.length; ++i) {
                    map.put(colNames[i], r.get(i));
                }
                list.add(map);
            }
            return list;
        } else {
            return Collections.emptyList();
        }
    }
}
