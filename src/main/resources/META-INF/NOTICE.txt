Apache Spark "all-in-one-jar"

This library includes Apache Spark and all its mandatory dependencies into a single jar. It is shaded to avoid dependency problems and optimized to avoid duplicated libraries with mule runtime.
This library is a re-package (see pom file for details) to make easier some kind of developments, but no changes are perform over any component code.

This product includes software developed at (among others)
The Apache Software Foundation (http://www.apache.org/).
Please, refer to license folder to view each component license.
